
Vue.component("searchbar",{
    props:["showall"],
    data:function(){
        return{
            inputText: ""
        }
    },
    template:`
        <div class="row d-flex justify-content-end mx-4 mt-5">
            <button class="btn btn-secondary" type="btn" id="btn-showall" v-if="!showall" v-on:click="onShowAllBtnClick">Show all</button><br>
            <div class="input-group col-sm-4">
                <input type="text" class="form-control" placeholder="Search" id="search-input" aria-describedby="btn-search" v-model="inputText"> 
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" id="btn-search" v-on:click="onSearchBtnClick">Search</button>
                </div>
            </div>
        </div>
    `,
    methods:{
        onSearchBtnClick: function(){
            this.$emit("search",this.inputText);
        },

        onShowAllBtnClick: function(){
            this.$emit("showall");
        }
    },
});

Vue.component("card",{
    props:['secObj'],
    template:`
        <div class="card p-1 mb-4 shadow" >
            <div class="card-body bg-white d-flex p-2 m-0">
                <span class="material-icons mx-1">info</span>
                <div class="d-flex flex-column d-inline-flex mx-1">
                    <div class="text-warning card-text sec-name" v-bind:title="secObj.securitydesc">{{secObj.securitydesc}}</div>
                    <div class="text-dark card-text sec-date">{{secObj.maturity_date}}</div>
                </div>
                <button type="button" class="btn btn-sm btn-success mx-2 btn-pick px-3 pt-0 pb-0 ml-auto">Pick +</button>
            </div>
            <hr class="m-0">

            <div class="row card-body">
                <div class="col">
                    <div class="text-muted info-type">ISIN</div>
                    <div class="info-type">{{secObj.isin}}</div>   
                </div>
                <div class="col">
                    <div class="text-muted info-type">COUPON TYPE</div>
                    <div class="info-type">{{secObj.coupon_type}}</div>
                </div>
                <div class="col">
                    <div class="text-muted info-type">#OFFER YIELD %</div>
                    <div class="info-type">{{secObj.offer_yield | yield}}</div>   
                </div>
            </div>

            <div class="row card-body">
                <div class="col">
                    <div class="text-muted info-type">INDIC BID/OFFER</div>
                    <div class="info-type">{{bidOffer}}</div>
                </div>
                <div class="col">
                    <div class="text-muted info-type">RATING*SP/M/F</div>
                    <div class="info-type">{{ratings}}</div>
                </div>
                <div class="col">
                    <div class="text-muted info-type">LOT SIZE</div>
                    <div class="info-type">{{secObj.lotsize}}</div> 
                </div>
            </div>
        </div>
    `,
    computed:{
        
        bidOffer: function(){
            let bidPrice = parseFloat(this.secObj.bid_price).toFixed(2);
            let offerPrice = parseFloat(this.secObj.offer_price).toFixed(2);
            
            return bidPrice + "/" + offerPrice;   
        },

        ratings:function(){

            let r1 = this.secObj.rating1 == "NONE" ?  "-" : this.secObj.rating1;
            let r2 = this.secObj.rating2 == "NONE" ?  "-" : this.secObj.rating2;
            let r3 = this.secObj.rating3 == "NONE" ?  "-" : this.secObj.rating3;

            return (r1 + "/" + r2 + "/" + r3);
        }
    },
    filters:{
        yield: function(value){
            return parseFloat(value).toFixed(2);
        }
    }
});


let app = new Vue({
    el: "#app",
    data:{
        modifiedSecurities: [],
        cardsToDisplay:[],
        allCardsVisible: false
    },

    methods:{
        
        prepareCardSearchData: function(secArr){  
            secArr.forEach(sec => {
                sec.yield = parseFloat(sec.offer_yield).toFixed(2);

                let bidPrice = parseFloat(sec.bid_price).toFixed(2);
                let offerPrice = parseFloat(sec.offer_price).toFixed(2);
                sec.bidOffer = bidPrice + "/" + offerPrice;

                let r1 = sec.rating1 == "NONE" ?  "-" : sec.rating1;
                let r2 = sec.rating2 == "NONE" ?  "-" : sec.rating2;
                let r3 = sec.rating3 == "NONE" ?  "-" : sec.rating3;

                sec.ratings = (r1 + "/" + r2 + "/" + r3);

                sec.valuesString = Object.values(sec).toString();

                this.modifiedSecurities.push(sec);
            });
        },
        
        searchCards:function(searchText){
            if(searchText != null && searchText != ""){
                this.cardsToDisplay = [];
                this.modifiedSecurities.forEach((val) => {
                    if(val.valuesString.indexOf(searchText) >= 0){
                        this.cardsToDisplay.push(val);
                    }
                })
                this.allCardsVisible = false;
            }
            
        },

        showAllCards: function(){
            this.cardsToDisplay = [];
            this.modifiedSecurities = [];
            axios.get("data")
            .then((response) => {
                this.prepareCardSearchData(response.data);
                this.cardsToDisplay = this.modifiedSecurities;
                this.allCardsVisible = true;
            });    
        }
    },

    created: function(){
        this.showAllCards();
    }

    
});


